# @copyright Copyright 2020, David BAILLY.
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# @file Universe.gd
# @author David BAILLY
# @date 27 Oct 2020
# @brief main scene for the demo
#
# @see https://gitlab.com/davibail/godot-dynamic-octree-lod

extends Spatial

const PRINT_TIME = false
var m_terrain_worker
var m_test_planet
var m_iteration = 0


func _ready():
	m_test_planet = Planet.new(123456789)
	self.add_child(m_test_planet)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	var time_start = OS.get_ticks_msec()
	var step = m_iteration % 4 + 1
	match (step):
		1:
			var player_translation = $Player.translation #todo transform coordinates
			m_test_planet.update_1(player_translation)
		2:
			m_test_planet.update_2()
		3:
			m_test_planet.update_3()
		4:
			m_test_planet.update_4()
	var time_stop = OS.get_ticks_msec()
	if (PRINT_TIME):
		print("update ",step," : ",time_stop-time_start)
	m_iteration += 1
