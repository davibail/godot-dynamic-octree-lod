# @copyright Copyright 2020, David BAILLY.
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# @file display_manager.gd
# @author David BAILLY
# @date 27 Oct 2020
# @brief api for the display of the cubes using the display_chunks
#
# @see https://gitlab.com/davibail/godot-dynamic-octree-lod

extends Node
class_name DisplayManager


var m_chunks = []
var m_chunk_size = 65536
var m_visited_cubes = 0
var m_blocked_cubes = 0
var m_visible_cubes = 0
var m_to_remove = []
var m_to_add = []
var m_coordinates_to_index = {}


func _ready():
	pass


func _process(_delta):
	while !m_to_add.empty():
		var data = m_to_add.pop_front()
		process_add_cube(data.pos,data.layer,data.color,data.coordinates)
	while !m_to_remove.empty():
		var data = m_to_remove.pop_front()
		process_remove_cube(data.coordinates,data.layer)
	


func _init():
	pass


func process_add_cube(pos, layer, color, coordinates):
	if (coordinates == 1073053696):
		print("added")
	var size = (1<<layer)
	var index = -1
	var chunk_index = 0
	for chunk in m_chunks:
		index = chunk.add(pos, size, color)
		if (index > -1):
			break
		chunk_index += 1
	
	if (index < 0):
		m_chunks.push_back(DisplayChunk.new(size, m_chunk_size))
		index = m_chunks[chunk_index].add(pos, size, color)
		self.call_deferred('add_child', m_chunks[chunk_index])
	
	index += m_chunk_size * chunk_index
	
	# return index of cube for tracking
	m_coordinates_to_index[str(coordinates)] = index


func process_remove_cube(coordinates, layer):
	if (coordinates == 1073053696):
		print("removed")
	var index = m_coordinates_to_index[str(coordinates)]
	var raw_index = index
	var chunk_index = 0
	
	while index >= m_chunk_size:
		index -= m_chunk_size
		chunk_index += 1
	
	m_chunks[chunk_index].remove(index, raw_index)
	m_coordinates_to_index.erase(str(coordinates))


func add_cube(pos, layer, color, coordinates):
	process_add_cube(pos, layer, color, coordinates)
	return
	m_to_add.push_back({"pos":pos,"layer":layer,"color":color,"coordinates":coordinates})


func remove_cube(coordinates, layer):
	process_remove_cube(coordinates, layer)
	return
	m_to_remove.push_back({"coordinates":coordinates,"layer":layer})
