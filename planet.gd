# @copyright Copyright 2020, David BAILLY.
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# @file planet.gd
# @author David BAILLY
# @date 27 Oct 2020
# @brief base class for generating a cuboid planet using cube_nodes
#
# @see https://gitlab.com/davibail/godot-dynamic-octree-lod

extends Node
class_name Planet


const PRINT_STATS = false
var m_generator
var m_display_manager
var m_root_chunk


func _ready():
	pass


func _process(_delta):
	pass


func _init(new_seed):
	m_generator = PlanetGenerator.new(new_seed)
	m_display_manager = DisplayManager.new()
	self.add_child(m_display_manager)
	var coordinates = 10<<58
	m_root_chunk = CubeNode.new(self,Vector3(0,0,0),coordinates)
	self.add_child(m_root_chunk)


func is_visible():
	return false


func has_children():
	return false


func update_1(player_translation):
	m_root_chunk.update_1(self, player_translation)


func update_2():
	m_root_chunk.update_2()


func update_3():
	m_root_chunk.update_3()


func update_4():
	m_display_manager.m_visited_cubes = 0
	m_display_manager.m_blocked_cubes = 0
	m_display_manager.m_visible_cubes = 0
	m_root_chunk.update_4(m_display_manager)
	if (PRINT_STATS):
		print(m_display_manager.m_visited_cubes, " ", m_display_manager.m_blocked_cubes, " ", m_display_manager.m_visible_cubes)
