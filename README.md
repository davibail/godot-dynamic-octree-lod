Working example of a octree cubic terrain loading.
The terrain is loaded with a resolution that depends on the distance to the player.
The closer to the player the smaller the cubes are.
There is a keymap to navigate that is set for a French keyboard. It can be changed in the menus of Godot Project.
