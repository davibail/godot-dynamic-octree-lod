# @copyright Copyright 2020, David BAILLY.
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# @file planet_generator.gd
# @author David BAILLY
# @date 27 Oct 2020
# @brief not fully functional planet generator helper class
#
# @see https://gitlab.com/davibail/godot-dynamic-octree-lod

extends Node
class_name PlanetGenerator


const MORTON_LUT = [
	{"x":-1,"y":-1,"z":-1},
	{"x":-1,"y":-1,"z":1},
	{"x":-1,"y":1,"z":-1},
	{"x":-1,"y":1,"z":1},
	{"x":1,"y":-1,"z":-1},
	{"x":1,"y":-1,"z":1},
	{"x":1,"y":1,"z":-1},
	{"x":1,"y":1,"z":1}]

#              3----7      011---111
#   z       1----5  |   001---101 |
#   | y     |  2-|--6    | 010-|-110
#   0---x   0----4      000---100

# Deterministic values
var m_coordinates
var m_seed
var m_terrain_noise

# Randomly generated values
var m_name

var m_core_radius
var m_mantle_radius
var m_ground_radius
var m_lower_atmosphere_radius
var m_upper_atmosphere_radius


func _ready():
	pass


func _init(new_seed):
	m_seed = new_seed
	
	m_terrain_noise = OpenSimplexNoise.new()
	m_terrain_noise.seed = m_seed
	m_terrain_noise.period = 31
	# m_terrain_noise.octaves = 6
	# m_terrain_noise.persistence = 0.5
	# m_terrain_noise.lacunarity = 3
	
	var rng = RandomNumberGenerator.new()
	rng.seed = m_seed
	m_name = 'test planet'
	
	# planet layers
	m_ground_radius = rng.randf_range(5000.0, 250000.0)
	m_mantle_radius = rng.randf_range(500.0, m_ground_radius)
	m_core_radius = rng.randf_range(50.0, m_mantle_radius)
	m_upper_atmosphere_radius = m_ground_radius * (1.0 + rng.randf())
	m_lower_atmosphere_radius = m_ground_radius + (m_upper_atmosphere_radius - m_ground_radius) * rng.randf()


func get_terrain_value(pos):
	return m_terrain_noise.get_noise_3dv(pos)


func get_terrain_data(pos, size):
	var data = {"type":0, "corners":[], "mean":0.0}
	var delta = size/2.0
	for i in range(8):
		var center = Vector3(pos.x + delta * MORTON_LUT[i].x, pos.y + delta * MORTON_LUT[i].y, pos.z + delta * MORTON_LUT[i].z)
		var value = m_terrain_noise.get_noise_3dv(center)
		data["corners"].push_back(value)
		data["mean"] += value
	data["mean"] /= 8
	
	if (data["mean"] > 0):
		data["type"] = 1
		
	return data
