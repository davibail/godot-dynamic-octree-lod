# @copyright Copyright 2020, David BAILLY.
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# @file display_chunk.gd
# @author David BAILLY
# @date 27 Oct 2020
# @brief container for the mesh cubes based on MultiMeshInstance
#
# @see https://gitlab.com/davibail/godot-dynamic-octree-lod

extends MultiMeshInstance
class_name DisplayChunk

const CORRECTION_FACTOR = 1.01

var m_size
var m_free_index = []
var m_ext2int = []
var m_int2ext = []


func _ready():
	pass


func _process(_delta):
	pass


func _init(cube_size, capacity):
	print('new chunk ', capacity, ' ', cube_size)
	
	m_size = cube_size
	for index in range(capacity):
		m_free_index.push_back(capacity - 1 - index)
		m_ext2int.push_back(-1)
		m_int2ext.push_back(-1)
	
	multimesh = MultiMesh.new()
	multimesh.transform_format = MultiMesh.TRANSFORM_3D
	multimesh.color_format = MultiMesh.COLOR_8BIT
	multimesh.custom_data_format = MultiMesh.CUSTOM_DATA_NONE
	
	multimesh.mesh = CubeMesh.new()
	multimesh.mesh.set_size(Vector3(CORRECTION_FACTOR * cube_size,CORRECTION_FACTOR * cube_size,CORRECTION_FACTOR * cube_size))
	
	var material = SpatialMaterial.new()
	material.set_flag(SpatialMaterial.FLAG_ALBEDO_FROM_VERTEX_COLOR , true)
	 
	material.albedo_color = Color(1,1,1)
	multimesh.mesh.surface_set_material(0, material)
	
	multimesh.instance_count = capacity
	multimesh.visible_instance_count = 0



func empty():
	return multimesh.visible_instance_count == 0



func add(pos, size, color):
	var ext_index = -1
	
	if (size == m_size and !m_free_index.empty()):
		ext_index = m_free_index.pop_back()
		var int_index = multimesh.visible_instance_count
		if (int_index < 0):
			print('Error: Chunk: add: int_index ', int_index)
		m_ext2int[ext_index] = int_index
		multimesh.set_instance_transform(int_index, Transform(Basis(), pos))
		multimesh.set_instance_color(int_index, color)
		m_ext2int[ext_index] = int_index
		m_int2ext[int_index] = ext_index
		multimesh.visible_instance_count += 1
		if (ext_index < 0):
			print('Error: Chunk: add: ext_index ', ext_index)
	
	return ext_index



func remove(ext_index, raw_index):
	if (ext_index >= 0 and ext_index < multimesh.instance_count):
		if (multimesh.visible_instance_count > 0):
			var int_index = m_ext2int[ext_index]
			var last_int_index = multimesh.visible_instance_count - 1
			var last_ext_index = m_int2ext[last_int_index]
			
			if (int_index > -1 and last_ext_index > -1):
				if (multimesh.visible_instance_count > 1):
					multimesh.set_instance_transform(int_index, multimesh.get_instance_transform(last_int_index))
					multimesh.set_instance_color(int_index, multimesh.get_instance_color(last_int_index))
					m_ext2int[last_ext_index] = int_index
					m_int2ext[int_index] = last_ext_index
				m_ext2int[ext_index] = -1
				m_int2ext[last_int_index] = -1
				multimesh.visible_instance_count -= 1
			else:
				print('Error: Chunk: remove: corrupted indexes (',
				'm_ext2int[',ext_index, '] = ', int_index,
				' m_int2ext[',last_int_index, '] = ', last_ext_index,')', raw_index)
		else:
			print('Error: Chunk: remove: already empty ',ext_index)
	else:
		print('Error: Chunk: remove: index out of range', ext_index)

