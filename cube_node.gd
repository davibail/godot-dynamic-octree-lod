# @copyright Copyright 2020, David BAILLY.
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# @file cube_node.gd
# @author David BAILLY
# @date 27 Oct 2020
# @brief base node for the octree generation
#
# @see https://gitlab.com/davibail/godot-dynamic-octree-lod
 
extends Node
class_name CubeNode

const R_MASK = 1 | 1 << 3 | 1 << 6 | 1 << 9
const G_MASK = 1 << 1 | 1 << 4 | 1 << 7 | 1 << 10
const B_MASK = 1 << 2 | 1 << 5 | 1 << 8 | 1 << 11
const LAYER_SHIFT = 58
const SOLID_THRESHOLD = -0.3
const RANGE_FACTOR = 16.0
const CLOSE_FACTOR = 8.0
const MORTON_LUT = [
	{"x":-1,"y":-1,"z":-1},
	{"x":-1,"y":-1,"z":1},
	{"x":-1,"y":1,"z":-1},
	{"x":-1,"y":1,"z":1},
	{"x":1,"y":-1,"z":-1},
	{"x":1,"y":-1,"z":1},
	{"x":1,"y":1,"z":-1},
	{"x":1,"y":1,"z":1}]
const OPPOSITES = {
	"x":"-x",
	"-x":"x",
	"y":"-y",
	"-y":"y",
	"z":"-z",
	"-z":"z"
}

#              3----7      011---111
#   z       1----5  |   001---101 |
#   | y     |  2-|--6    | 010-|-110
#   0---x   0----4      000---100

# passed at init
var m_center = Vector3()
var m_size
var m_layer
var m_coordinates
var m_morton_index

# derived from paramters
var m_data
var m_has_children = false
var m_children = []
var m_neighbours = {}

# cube display
var m_is_visible = false
var m_is_hidden = false
var m_has_cube = false
var m_in_range = false
var m_too_close = false
var m_all_children_in_range = false

var m_lod_changed = true
var m_neighbours_changed = false
var m_cubes_changed = false
var m_self_destruct = false
var m_killed = false

var m_blocked = {
	"x":false,
	"-x":false,
	"y":false,
	"-y":false,
	"z":false,
	"-z":false
}
var m_all_blocked = false

var m_blocking = {
	"x":false,
	"-x":false,
	"y":false,
	"-y":false,
	"z":false,
	"-z":false
}

func _ready():
	pass


func _init(planet, center, coordinates):
	m_center.x = center.x
	m_center.y = center.y
	m_center.z = center.z
	m_coordinates = coordinates
	m_layer = coordinates>>LAYER_SHIFT
	m_morton_index = (coordinates>>(m_layer*3))&7
	m_size = 1<<m_layer
	m_data = planet.m_generator.get_terrain_data(m_center,m_size)


func get_color():
	match (m_layer%3):
		0:
			return Color(1,0,0)
		1: 
			return Color(0,1,0)
		2:
			return Color(0,0,1)
	return Color(1,1,1)


func get_morton_opposite(direction):
	match direction:
		"x", "-x":
			if (m_morton_index & 4):
				return m_morton_index & 3
			else:
				return m_morton_index | 4
		"y", "-y":
			if (m_morton_index & 2):
				return m_morton_index & 5
			else:
				return m_morton_index | 2
		"z", "-z":
			if (m_morton_index & 1):
				return m_morton_index & 6
			else:
				return m_morton_index | 1
	return m_morton_index


func has_children_cube():
	return m_has_children


func get_child_cube(morton):
	if (m_has_children):
		return m_children[morton]
	return self


func tell_neighbour(direction):
	if (direction in m_neighbours):
		m_neighbours[direction].m_neighbours_changed = true


func tell_all_neighbours():
	for key in m_neighbours:
		m_neighbours[key].m_neighbours_changed = true


func set_neighbour(neighbour, direction):
	if (!is_instance_valid(neighbour)):
		print("invalid neighbour")
		m_neighbours.erase(direction)
	else:
		if (neighbour.m_self_destruct):
			print("dead neighbour")
			m_neighbours.erase(direction)
		else:
			m_neighbours[direction] = neighbour
	m_neighbours_changed = true


func has_neighbour(direction):
	if (direction in m_neighbours):
		return true
	return false


func get_neighbour(direction):
	if (direction in m_neighbours):
		return m_neighbours[direction]
	return null


func remove_neighbour(direction):
	m_neighbours.erase(direction)


func is_blocking(direction):
	return m_blocking[direction]


func is_visible():
	return m_is_hidden


func generate_child_coordinates(index):
	var coordinates = m_coordinates
	coordinates = (coordinates<<6)>>6 # remove the layer indicator
	var child_layer = m_layer-1
	coordinates = coordinates | (child_layer<<LAYER_SHIFT) # insert the child layer
	coordinates = coordinates>>(child_layer*3)
	coordinates = coordinates | index
	coordinates = coordinates<<(child_layer*3)
	return coordinates


func split(planet):
	if (m_layer > 0 and !m_has_children):
		m_children = []
		var delta = m_size/4.0
		for i in range(8):
			var center = Vector3(m_center.x + delta * MORTON_LUT[i].x, m_center.y + delta * MORTON_LUT[i].y, m_center.z + delta * MORTON_LUT[i].z)
			m_children.push_back(load(get_script().resource_path).new(planet, center, generate_child_coordinates(i)))
		m_has_children = true
		for i in range(8):
			self.add_child(m_children[i])
			if (i & 4):
				if ("x" in m_neighbours):
					m_children[i].set_neighbour(m_neighbours["x"],"x")
				m_children[i].set_neighbour(m_children[i & 3],"-x")
			else:
				if ("-x" in m_neighbours):
					m_children[i].set_neighbour(m_neighbours["-x"],"-x")
				m_children[i].set_neighbour(m_children[i | 4],"x")
				
			if (i & 2):
				if ("y" in m_neighbours):
					m_children[i].set_neighbour(m_neighbours["y"],"y")
				m_children[i].set_neighbour(m_children[i & 5],"-y")
			else:
				if ("-y" in m_neighbours):
					m_children[i].set_neighbour(m_neighbours["-y"],"-y")
				m_children[i].set_neighbour(m_children[i | 2],"y")
				
			if (i & 1):
				if ("z" in m_neighbours):
					m_children[i].set_neighbour(m_neighbours["z"],"z")
				m_children[i].set_neighbour(m_children[i & 6],"-z")
			else:
				if ("-z" in m_neighbours):
					m_children[i].set_neighbour(m_neighbours["-z"],"-z")
				m_children[i].set_neighbour(m_children[i | 1],"z")
		self.tell_all_neighbours()


func unsplit():
	if (m_has_children):
		for child in m_children:
			child.kill()
		m_has_children = false


func show(display_manager):
	if (!m_has_cube and m_data.mean > SOLID_THRESHOLD):
		display_manager.add_cube(m_center, m_layer, get_color(), m_coordinates)
		m_has_cube = true
		m_cubes_changed = true


func hide(display_manager):
	if (m_has_cube):
		display_manager.remove_cube(m_coordinates, m_layer)
		m_has_cube = false
		m_cubes_changed = true


func  kill():
	self.unsplit()
	m_killed = true


func update_blocking():
	if (m_has_children and !m_is_visible):
		m_blocking = {
			"x":true,
			"-x":true,
			"y":true,
			"-y":true,
			"z":true,
			"-z":true
		}
		for i in range(8):
			if (i & 4):
				m_blocking["x"] = m_blocking["x"] and m_children[i].m_blocking["x"]
			else:
				m_blocking["-x"] = m_blocking["-x"] and m_children[i].m_blocking["-x"]
			if (i & 2):
				m_blocking["y"] = m_blocking["y"] and m_children[i].m_blocking["y"]
			else:
				m_blocking["-y"] = m_blocking["-y"] and m_children[i].m_blocking["-y"]
			if (i & 1):
				m_blocking["z"] = m_blocking["z"] and m_children[i].m_blocking["z"]
			else:
				m_blocking["-z"] = m_blocking["-z"] and m_children[i].m_blocking["-z"]
		
		for key in m_blocking:
			tell_neighbour(key)
	else:
		if (m_data.mean > SOLID_THRESHOLD):
			for key in m_blocking:
				if (!m_blocking[key]):
					tell_neighbour(key)
			# if node is solid then it blocks all faces
			# TODO depending of the type of cube some faces might not be blocking
			# ex : slopes
			m_blocking = {
				"x":true,
				"-x":true,
				"y":true,
				"-y":true,
				"z":true,
				"-z":true
			}
		else:
			for key in m_blocking:
				if (m_blocking[key]):
					tell_neighbour(key)
			m_blocking = {
				"x":false,
				"-x":false,
				"y":false,
				"-y":false,
				"z":false,
				"-z":false
			}


func update_blocked():
	m_blocked = {
		"x":false,
		"-x":false,
		"y":false,
		"-y":false,
		"z":false,
		"-z":false
	}
	for key in m_neighbours:
		m_blocked[key] = m_neighbours[key].m_blocking[OPPOSITES[key]]
	m_all_blocked = true
	for key in m_blocked:
		m_all_blocked = m_all_blocked and m_blocked[key]


func update_lod(planet, player_translation):
	m_lod_changed = false
	m_in_range = false
	m_too_close = false
	
	var distance = m_center.distance_to(player_translation)
	
	# If the player is in range
	if (distance < RANGE_FACTOR * m_size):
		m_in_range = true
	
	# If the player is close enough then split current chunk
	if (distance < CLOSE_FACTOR * m_size):
		m_too_close = true
	
	# If too close materialize children
	if (m_too_close and !m_has_children and !m_all_blocked):
		self.split(planet)
		m_lod_changed = true
	
	# If too far children can be free
	if (!m_in_range and m_has_children):
		self.unsplit()
		m_lod_changed = true
	
	if (m_has_children):
		for child in m_children:
			if (child.m_lod_changed):
				m_lod_changed = true


func update_neighbours():
	if (m_killed):
		for key in m_neighbours:
			if (m_neighbours[key].m_layer <= m_layer):
				m_neighbours[key].set_neighbour(self.get_parent(), OPPOSITES[key])
		self.tell_all_neighbours()
	else:
		for key in m_neighbours:
			if (m_neighbours[key].m_layer > m_layer and m_neighbours[key].has_children_cube()):
				m_neighbours[key] = m_neighbours[key].get_child_cube(self.get_morton_opposite(key))


func update_display(display_manager):
	# Updates the display of the cube is needed
	m_cubes_changed = false
	if (m_killed):
		self.hide(display_manager)
	else:
		if (m_in_range and !m_all_children_in_range and !self.get_parent().is_visible()):
			if (!m_is_visible and !m_all_blocked):
				self.show(display_manager)
				m_is_visible = true
				m_cubes_changed = true
		else:
			if (m_is_visible):
				self.hide(display_manager)
				m_is_visible = false
				m_cubes_changed = true
		
		display_manager.m_visited_cubes += 1
		if (m_all_blocked):
			display_manager.m_blocked_cubes += 1
		if (m_is_visible):
			display_manager.m_visible_cubes += 1


func update_logic():
	pass


func update_1(planet,player_translation):
	m_neighbours_changed = false
	if (m_killed):
		self.unsplit()
		self.get_parent().remove_child(self)
		if (m_has_cube):
			print("still has cube")
		self.queue_free()

	if (m_has_children):
		for child in m_children:
			child.update_1(planet,player_translation)
	self.update_lod(planet,player_translation)


func update_2():
	m_all_children_in_range = false
	if (m_has_children):
		m_all_children_in_range = true
		for child in m_children:
			m_all_children_in_range = m_all_children_in_range and child.m_in_range
			child.update_2()
	if (m_neighbours_changed or m_killed):
		self.update_neighbours()
	if (m_lod_changed or m_cubes_changed):
		self.update_blocking()


func update_3():
	if (m_has_children):
		for child in m_children:
			child.update_3()
	if (m_neighbours_changed):
		self.update_blocked()
	self.update_logic()


func update_4(display_manager):
	m_cubes_changed = false
	if (m_has_children):
		for child in m_children:
			child.update_4(display_manager)
	self.update_display(display_manager)
